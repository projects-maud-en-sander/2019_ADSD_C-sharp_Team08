﻿using _2019_ADSD_C_sharp_Team08.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _2019_ADSD_C_sharp_Team08.Controllers
{
    public class HomeController : Controller
    {
        private API _api = new API();

        public ActionResult Index()
        {
            //Get a random joke from the api
            var Joke = JsonConvert.DeserializeObject<JokeModel>(_api.Get("food/jokes/random?apiKey="));

            //Get a random fact from the api
            var Trivia = JsonConvert.DeserializeObject<TriviaModel>(_api.Get("food/trivia/random?apiKey="));

            ViewBag.Joke = Joke.Text;
            ViewBag.Trivia = Trivia.Text;

            return View();
        }

        
        public ActionResult About()
        {

            return View();
        }


    }
}