﻿using _2019_ADSD_C_sharp_Team08.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _2019_ADSD_C_sharp_Team08.Controllers
{
    public class RecipeController : Controller
    {
        private API _api = new API();


        // GET: Recipe
        public ActionResult Index(string[] checkbox)
        {
            //If nothing was checked
            if(checkbox == null)
            {
                //go back to the index page
                return RedirectToAction("Index", "UserHasIngredients");
            }
            else
            {
                //if the checkbox doesnt send a value the value is on
                if(checkbox.Contains("on") == true)
                {
                    //go back to the indexpage
                    return RedirectToAction("Index", "UserHasIngredients");
                }
                //Array to string for api call
                string ingredients = string.Join(",+", checkbox);

                //get all recipes with checked ingredients
                var allRecipes = JsonConvert.DeserializeObject<List<RecipeByIngredientModel>>(_api.Get("recipes/findByIngredients?ingredients="+ ingredients + "&number=5&ranking=1&apiKey="));

                return View(allRecipes);
            }
            
        }

        // GET: Recipe/Details/5
        public ActionResult Details(int id)
        {
            //get recipe information with id
            RecipeInformationModel RecipeInformation = JsonConvert.DeserializeObject<RecipeInformationModel>(_api.Get("/recipes/"+id+"/information?includeNutrition=false&apiKey="));

            return View(RecipeInformation);
        }



    }
}
