﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _2019_ADSD_C_sharp_Team08.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace _2019_ADSD_C_sharp_Team08.Controllers
{
    [Authorize]
    public class UserHasIngredientsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //get the Api class
        private API _api = new API();

        // GET: UserHasIngredients
        public async Task<ActionResult> Index()
        {
            //get user id
            string UserId = User.Identity.GetUserId().ToString();

            //get the ingredients from user
            var ingredients = from ii in db.UserHasIngredients
                     where
                     ii.userId == UserId
                     select ii;

            //make a tuple from ingredient model and userhasingredient 
            List<Tuple<IngredientModel, UserHasIngredient>> listIngredients = new List<Tuple<IngredientModel, UserHasIngredient>>();
            foreach  (UserHasIngredient ingredient in ingredients)
            {
                //search ingredient by id
                IngredientModel im = JsonConvert.DeserializeObject<IngredientModel>(_api.Get("/food/ingredients/"+ ingredient.ingredientId + "/information?apiKey="));
                
                //add ingredient information and user ingredient information to tuple
                listIngredients.Add(new Tuple <IngredientModel, UserHasIngredient>(im, ingredient ));
            }

            return View(listIngredients);
        }


        // GET: UserHasIngredients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserHasIngredients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserHasIngredient userHasIngredient)
        {
            //add ingriedient id to userhasingredient
            var ingredient_id = Request.Form["ingredient_id"];
            userHasIngredient.ingredientId = Int32.Parse(ingredient_id);

            //add amount id to userhasingredient
            var amount = Request.Form["amount"];
            userHasIngredient.amount = double.Parse(amount);

            //add ingriedient unit to userhasingredient
            var unit = Request.Form["unit"];
            userHasIngredient.unit = unit;

            //add ingriedient user id to userhasingredient
            userHasIngredient.userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.UserHasIngredients.Add(userHasIngredient);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(userHasIngredient);
        }

        // GET: UserHasIngredients/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //find the record with id
            UserHasIngredient userHasIngredient = await db.UserHasIngredients.FindAsync(id);

            //if it is not empty
            if (userHasIngredient == null)
            {
                return HttpNotFound();
            }
            return View(userHasIngredient);
        }

        // POST: UserHasIngredients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            UserHasIngredient userHasIngredient = await db.UserHasIngredients.FindAsync(id);
            db.UserHasIngredients.Remove(userHasIngredient);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
