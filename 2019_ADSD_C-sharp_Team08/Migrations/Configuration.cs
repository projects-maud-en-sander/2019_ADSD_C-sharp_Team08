﻿namespace _2019_ADSD_C_sharp_Team08.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_2019_ADSD_C_sharp_Team08.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "_2019_ADSD_C_sharp_Team08.Models.ApplicationDbContext";
        }

        protected override void Seed(_2019_ADSD_C_sharp_Team08.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
