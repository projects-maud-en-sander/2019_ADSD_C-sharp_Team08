﻿namespace _2019_ADSD_C_sharp_Team08.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserHasIngredient : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserHasIngredients",
                c => new
                    {
                        userHasIngredientId = c.Long(nullable: false, identity: true),
                        userId = c.Long(nullable: false),
                        ingredientId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.userHasIngredientId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserHasIngredients");
        }
    }
}
