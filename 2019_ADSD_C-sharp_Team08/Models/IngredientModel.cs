﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
   
    public class IngredientModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("aisle")]
        public string Aisle { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("meta")]
        public object[] Meta { get; set; }

        [JsonProperty("original")]
        public object Original { get; set; }

        [JsonProperty("originalName")]
        public object OriginalName { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("unitLong")]
        public string UnitLong { get; set; }

        [JsonProperty("unitShort")]
        public string UnitShort { get; set; }

        //Not in RecipeByIngredient Ingredients
        [JsonProperty("consistency")]
        public string Consistency { get; set; }

        [JsonProperty("estimatedCost")]
        public EstimatedCost EstimatedCost { get; set; }

        [JsonProperty("shoppingListUnits")]
        public string[] ShoppingListUnits { get; set; }

        [JsonProperty("possibleUnits")]
        public string[] PossibleUnits { get; set; }

    }

    public class EstimatedCost
    {
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }

}