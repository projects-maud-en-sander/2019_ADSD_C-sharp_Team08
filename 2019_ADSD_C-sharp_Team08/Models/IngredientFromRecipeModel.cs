﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
    public class IngredientFromRecipeModel : IngredientModel
    {
        [JsonProperty("measures")]
        public Measures Measures { get; set; }


    }


}