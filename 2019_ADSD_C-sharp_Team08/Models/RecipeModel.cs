﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
    public class RecipeModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("imageType")]
        public string ImageType { get; set; }

    }
}