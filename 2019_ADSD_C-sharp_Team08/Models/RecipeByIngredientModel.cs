﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
    public class RecipeByIngredientModel : RecipeModel
    {
        [JsonProperty("likes")]
        public long Likes { get; set; }

        [JsonProperty("missedIngredientCount")]
        public long MissedIngredientCount { get; set; }

        [JsonProperty("missedIngredients")]
        public IngredientModel[] MissedIngredients { get; set; }

        [JsonProperty("unusedIngredients")]
        public IngredientModel[] UnusedIngredients { get; set; }

        [JsonProperty("usedIngredientCount")]
        public long UsedIngredientCount { get; set; }

        [JsonProperty("usedIngredients")]
        public IngredientModel[] UsedIngredients { get; set; }
    }
}