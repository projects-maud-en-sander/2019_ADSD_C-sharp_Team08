﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
    public class UserHasIngredient
    {
        [Key]
        public long userHasIngredientId { get; set; }

        // Foreign key 
        [Display(Name = "AspNetUsers")]
        public string userId { get; set; }

        [ForeignKey("userId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public long ingredientId { get; set; }

        public double amount { get; set; }

        public string unit { get; set; }

    }
}