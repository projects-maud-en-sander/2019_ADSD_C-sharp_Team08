﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08.Models
{
    public class RecipeInformationModel : RecipeModel
    {
        [JsonProperty("servings")]
        public long Servings { get; set; }

        [JsonProperty("readyInMinutes")]
        public long ReadyInMinutes { get; set; }

        [JsonProperty("license")]
        public string License { get; set; }

        [JsonProperty("sourceName")]
        public string SourceName { get; set; }

        [JsonProperty("sourceUrl")]
        public Uri SourceUrl { get; set; }

        [JsonProperty("spoonacularSourceUrl")]
        public Uri SpoonacularSourceUrl { get; set; }

        [JsonProperty("aggregateLikes")]
        public long AggregateLikes { get; set; }

        [JsonProperty("healthScore")]
        public long HealthScore { get; set; }

        [JsonProperty("spoonacularScore")]
        public long SpoonacularScore { get; set; }

        [JsonProperty("pricePerServing")]
        public double PricePerServing { get; set; }

        [JsonProperty("analyzedInstructions")]
        public object[] AnalyzedInstructions { get; set; }

        [JsonProperty("cheap")]
        public bool Cheap { get; set; }

        [JsonProperty("creditsText")]
        public string CreditsText { get; set; }

        [JsonProperty("cuisines")]
        public object[] Cuisines { get; set; }

        [JsonProperty("dairyFree")]
        public bool DairyFree { get; set; }

        [JsonProperty("diets")]
        public object[] Diets { get; set; }

        [JsonProperty("gaps")]
        public string Gaps { get; set; }

        [JsonProperty("glutenFree")]
        public bool GlutenFree { get; set; }

        [JsonProperty("instructions")]
        public string Instructions { get; set; }

        [JsonProperty("ketogenic")]
        public bool Ketogenic { get; set; }

        [JsonProperty("lowFodmap")]
        public bool LowFodmap { get; set; }

        [JsonProperty("occasions")]
        public object[] Occasions { get; set; }

        [JsonProperty("sustainable")]
        public bool Sustainable { get; set; }

        [JsonProperty("vegan")]
        public bool Vegan { get; set; }

        [JsonProperty("vegetarian")]
        public bool Vegetarian { get; set; }

        [JsonProperty("veryHealthy")]
        public bool VeryHealthy { get; set; }

        [JsonProperty("veryPopular")]
        public bool VeryPopular { get; set; }

        [JsonProperty("whole30")]
        public bool Whole30 { get; set; }

        [JsonProperty("weightWatcherSmartPoints")]
        public long WeightWatcherSmartPoints { get; set; }

        [JsonProperty("dishTypes")]
        public string[] DishTypes { get; set; }

        [JsonProperty("extendedIngredients")]
        public IngredientFromRecipeModel[] ExtendedIngredients { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("winePairing")]
        public WinePairing WinePairing { get; set; }
    }

    public class Measures
    {
        [JsonProperty("metric")]
        public Metric Metric { get; set; }

        [JsonProperty("us")]
        public Metric Us { get; set; }
    }

    public class Metric
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("unitLong")]
        public string UnitLong { get; set; }

        [JsonProperty("unitShort")]
        public string UnitShort { get; set; }
    }

    public class WinePairing
    {
        [JsonProperty("pairedWines")]
        public string[] PairedWines { get; set; }

        [JsonProperty("pairingText")]
        public string PairingText { get; set; }

        [JsonProperty("productMatches")]
        public ProductMatch[] ProductMatches { get; set; }
    }

    public class ProductMatch
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("averageRating")]
        public double AverageRating { get; set; }

        [JsonProperty("ratingCount")]
        public long RatingCount { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("link")]
        public Uri Link { get; set; }
    }

}