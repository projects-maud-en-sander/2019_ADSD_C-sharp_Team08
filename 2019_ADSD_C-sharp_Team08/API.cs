﻿using _2019_ADSD_C_sharp_Team08.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace _2019_ADSD_C_sharp_Team08
{
    public class API
    {
        //httpClient
        public HttpClient httpClient;
        //http response
        HttpResponseMessage httpResponseMessage;

        public string ApiKey;
        public string ApiPath;

        //construct function
        public API()
        {
            //make new client
            httpClient = new HttpClient();
            //the adress that is always the same
            httpClient.BaseAddress = new Uri("https://api.spoonacular.com");
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));


        }

        public string Get(string ApiPath)
        {
            //see if the apikey should be send with ? or & in front of it
            ApiKey = "b5edc92d105a42c986acc6bda72f6d60";

            try { 
                //get the values from api with apipath and apikey
                httpResponseMessage = httpClient.GetAsync(ApiPath+ApiKey).Result;

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //return the result
                    return httpResponseMessage.Content.ReadAsStringAsync().Result;
                }else{
                    Log.LogEvent("Ops! Something went wrong: "+httpResponseMessage.ToString());
                    throw new HttpException(500, "Ops, something went wrong! : "+ httpResponseMessage.ToString());

                }
            }
            catch(Exception e){
                 Log.LogEvent("Ops! Something went wrong: " + httpResponseMessage.ToString());
                 throw new HttpException(500, "Ops, something went wrong! : "+e.Message);


            }
        }
    }
}