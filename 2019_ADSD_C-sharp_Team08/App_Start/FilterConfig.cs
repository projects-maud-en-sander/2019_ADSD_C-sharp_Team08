﻿using System.Web;
using System.Web.Mvc;

namespace _2019_ADSD_C_sharp_Team08
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
