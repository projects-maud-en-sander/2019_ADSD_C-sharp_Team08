﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_2019_ADSD_C_sharp_Team08.Startup))]
namespace _2019_ADSD_C_sharp_Team08
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
